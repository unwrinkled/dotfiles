local augroup = vim.api.nvim_create_augroup -- Create/get autocommand group
local autocmd = vim.api.nvim_create_autocmd -- Create autocommand

-- Highlight on yank
augroup('YankHighlight', { clear = true })
autocmd('TextYankPost', {
	group = 'YankHighlight',
	callback = function()
		vim.highlight.on_yank({ higroup = 'IncSearch', timeout = '1000' })
	end
})

-- Remove whitespace on save
autocmd('BufWritePre', {
	pattern = '',
	command = ":%s/\\s\\+$//e"
})

-- Autocommand that reloads neovim whenever you save the packer_init.lua file
augroup('packer_user_config', { clear = true })
autocmd('BufWritePost', {
	group = 'packer_user_config',
	pattern = vim.fn.stdpath('config'):gsub('\\', '/') .. '/lua/plugins/init.lua',
	desc = 'Automatically call PackerSync when plugins/init.lua is saved.',
	command = 'source <afile> | PackerSync'
})

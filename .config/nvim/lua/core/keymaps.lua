local function map(mode, lhs, rhs, opts)
	local options = { noremap = true, silent = true }
	if opts then
		options = vim.tbl_extend('force', options, opts)
	end
	vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

vim.g.mapleader = ','

map('i', 'jj', '<Esc>')

map('n', '<leader>c', ':nohl<CR>')

map('n', '<C-h>', '<C-w>h')
map('n', '<C-j>', '<C-w>j')
map('n', '<C-k>', '<C-w>k')
map('n', '<C-l>', '<C-w>l')

-- Plugins

-- NvimTree
map('n', '<C-n>', ':NvimTreeToggle<CR>') -- open/close
map('n', '<leader>n', ':NvimTreeFindFile<CR>') -- search file

-- LSP
map('n', '<space>e', ':lua vim.diagnostic.open_float()<CR>')
map('n', '[d', ':lua vim.diagnostic.goto_prev()<CR>')
map('n', ']d', ':lua vim.diagnostic.goto_next()<CR>')
map('n', '<space>q', ':lua vim.diagnostic.setloclist()<CR>')

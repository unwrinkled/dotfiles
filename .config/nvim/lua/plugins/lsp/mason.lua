local mason_ok, mason = pcall(require, 'mason')
if not mason_ok then
	return
end

mason.setup {
	max_concurrent_installers = 1,

	ui = {
        icons = {
            package_installed = "✓",
            package_pending = "➜",
            package_uninstalled = "✗",
        },
    },
}

local mason_lsp_ok, mason_lsp = pcall(require, 'mason-lspconfig')
if not mason_lsp_ok then
	return
end

mason_lsp.setup {
	ensure_installed = { 'sumneko_lua', 'clangd' },
	automatic_installation = true,
}

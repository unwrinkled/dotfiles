local luasnip_ok, luasnip = pcall(require, 'luasnip')
if not luasnip_ok then
	return
end

local options = {
	history = true,
	updateevents = "TextChanged, TextChangedI",
}
luasnip.config.set_config(options)

require("luasnip.loaders.from_vscode").lazy_load()

local status, term = pcall(require, "toggleterm")
if not status then
	error("Error at toggleterm config.")
end

term.setup{
	open_mapping = [[<c-\>]],
	hide_numbers = true,
	shade_terminals = true,
	start_in_insert = true,
	insert_mappings = true,
	terminal_mappings = true,
	persist_size = true,
	persist_mode = true,
	direction = "float",
	auto_scroll = true,
	float_opts = {
		border = 'curved',
	},
}

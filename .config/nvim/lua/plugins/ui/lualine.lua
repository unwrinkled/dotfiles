local lualine_ok, lualine = pcall(require, 'lualine')
if not lualine_ok then
	return
end

local config = {

}

lualine.setup(config)

-- Automatically install packer
local fn = vim.fn
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
local packer_bootstrap = nil

if fn.empty(fn.glob(install_path)) > 0 then
	packer_bootstrap = fn.system({
		'git',
		'clone',
		'--depth',
		'1',
		'https://github.com/wbthomason/packer.nvim',
		install_path
	})
	vim.o.runtimepath = vim.fn.stdpath('data') .. '/site/pack/*/start/*,' .. vim.o.runtimepath
end

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, 'packer')
if not status_ok then
	return
end

return packer.startup(function(use)
	use 'wbthomason/packer.nvim'

	use {
		'navarasu/onedark.nvim',
		config = "require('plugins.onedark')"
	}

	use {
		'kyazdani42/nvim-tree.lua',
		requires = {
			'kyazdani42/nvim-web-devicons',
		},
		config = "require('plugins.ui.nvim-tree')"
	}

	use {
		'windwp/nvim-autopairs',
		config = "require('plugins.autopairs')",
	}

	use {
		'neovim/nvim-lspconfig',
		requires = {
			'williamboman/mason.nvim',
			'williamboman/mason-lspconfig.nvim',
			'hrsh7th/cmp-nvim-lsp',
		},
		config = "require('plugins.lsp.lspconfig')"
	}

	use {
		'hrsh7th/nvim-cmp',
		requires = {
			'saadparwaiz1/cmp_luasnip',
			'L3MON4D3/LuaSnip',
			'saadparwaiz1/cmp_luasnip',
			'hrsh7th/cmp-buffer',
			--'onsails/lspkind.nvim',
		},
		config = "require('plugins.lsp.cmp')",
	}

	use {
		'L3MON4D3/LuaSnip',
		requires = {
			'rafamadriz/friendly-snippets'
		},
		config = "require('plugins.lsp.luasnip')",
	}

	use {
		'nvim-treesitter/nvim-treesitter',
		config = "require('plugins.treesitter')"
	}

	use {
		'lukas-reineke/indent-blankline.nvim',
		config = "require('plugins.ui.indentline')",
	}

	use {
		'folke/todo-comments.nvim',
		requires = 'nvim-lua/plenary.nvim',
		config = "require('plugins.ui.todo-comments')",
	}

	use {
		'nvim-lualine/lualine.nvim',
		requires = { 'kyazdani42/nvim-web-devicons', opt = true },
		config = "require('plugins.ui.lualine')",
	}

	use {
		'lewis6991/gitsigns.nvim',
		config = "require('plugins.utils.gitsigns')",
	}

	use {
		'numToStr/Comment.nvim',
		config = function() require('Comment').setup() end
	}

	use {
		'folke/which-key.nvim',
		config = "require('plugins.utils.which-key')",
	}

	use {
		'akinsho/toggleterm.nvim',
		config = "require('plugins.ui.terminal')",
	}

	if packer_bootstrap then
		require('packer').sync()
	end
end)

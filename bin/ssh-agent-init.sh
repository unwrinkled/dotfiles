incr-num-terms ()
{
	if [[ -f "$HOME/.ssh/nterms" ]]; then
		NUM_TERMS="$(cat $HOME/.ssh/nterms)"
	else
		NUM_TERMS="0"
	fi
	echo $(($NUM_TERMS + 1)) > "$HOME/.ssh/nterms"
}

incr-num-terms

pgrep -u "$USER" -x ssh-agent > /dev/null || {
	ssh-agent | sed "s/^echo/#echo/" > "$HOME/.ssh/agent-env"
}

if [[ ! "$SSH_AUTH_SOCK" ]]; then
	. "$HOME/.ssh/agent-env"
fi

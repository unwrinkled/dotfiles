NUM_TERMS="$(<$HOME/.ssh/nterms)"
NUM_TERMS=$(($NUM_TERMS - 1))
echo $NUM_TERMS > "$HOME/.ssh/nterms"

# If all terminals are closed
if [[ "$NUM_TERMS" -eq "0" ]]; then
	# If ssh-agent is open kill it
	if pgrep -u "$USER" -x ssh-agent > /dev/null; then
		pkill -u "$USER" -x ssh-agent
	fi

	if [[ -f "$HOME/.ssh/agent-env" ]]; then
		# Remove ssh-agent environment
		rm "$HOME/.ssh/agent-env"
	fi

	if [[ -f "$HOME/.ssh/nterms" ]]; then
		# Remove number of terminals file
		rm "$HOME/.ssh/nterms"
	fi
fi
